<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->head['datapegawai'] = $this->M_pegawai->selectById($_SESSION['id_pegawai'])->row_array();		

	}

	public function index()
	{
		if(!$this->session->userdata('logged_in')) {
			redirect('login/');
		}else{
			$data['d_ec'] = $this->M_ec->selectAll();
			$data['d_pegawai'] = $this->M_pegawai->selectAll();
			$data['d_periode'] = $this->M_periode->selectAll()->result_array();	
			print_r($data['d_periode'])	;
			$this->load->view('layout/aheader', $this->head);
			$this->load->view('admin/ec_pegawai_view', $data);
			$this->load->view('layout/afooter');
		}
	}

	public function ecAct($comm){
		if(!$this->session->userdata('logged_in')) {
			redirect('login/');
		}else{
			if($comm == "add"){
				$this->load->view('layout/aheader', $this->head);
				$this->load->view('admin/ec_detail_pegawai_view');
				$this->load->view('layout/afooter');
			}else if($comm == "add2"){
				$this->load->view('layout/aheader', $this->head);
				$this->load->view('admin/ec_edit_pegawai_view');
				$this->load->view('layout/afooter');		
			}else if($comm == "add3"){
				$data = $this->input->post();
				var_dump($data);
				print_r($data);
				$this->M_periode->insert($data);
				$this->load->view('layout/aheader', $this->head);
				$this->load->view('admin/ec_detail_pegawai_view');
				$this->load->view('layout/afooter');
			}
		}
	}

	public function spjAct($comm){
		if(!$this->session->userdata('logged_in')) {
			redirect('login/');
		}else{
			if($comm == "add"){
				$this->load->view('layout/aheader', $this->head);
				$this->load->view('admin/spj_detail_pegawai_view');
				$this->load->view('layout/afooter');
			}else if($comm == "add2"){
				$this->load->view('layout/aheader', $this->head);
				$this->load->view('admin/spj_edit_pegawai_view');
				$this->load->view('layout/afooter');	
			}
		}
		
	}


	public function btAct($comm){
		if(!$this->session->userdata('logged_in')) {
			redirect('login/');
		}else{
			if($comm == "add"){
				$this->load->view('layout/aheader', $this->head);
				$this->load->view('admin/bt_detail_pegawai_view');
				$this->load->view('layout/afooter');
			}else if($comm == "add2"){
				$this->load->view('layout/aheader', $this->head);
				$this->load->view('admin/bt_acc_pegawai_view');
				$this->load->view('layout/afooter');
			}
		}
		
	}

	
}