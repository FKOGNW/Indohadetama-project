<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	//private $head;
	public function __construct(){
		parent::__construct();
		$this->head['datapegawai'] = $this->M_pegawai->selectById($_SESSION['id_pegawai'])->row_array();		
		//$this->head['segment'] = $this->uri->segment(1);
		//$this->head['segment'] = $this->uri->segment(2);
		//$test = $_SESSION['id_pegawai'];
		//var_dump($this->head);
		//var_dump($test);
	}

	public function index()
	{
		if(!$this->session->userdata('logged_in')) {
			redirect('login/');
		}else{
			$this->load->view('layout/pheader', $this->head);
			$this->load->view('pegawai/home_view');
			$this->load->view('layout/pfooter');
		}
	}

	/*for load ec view*/
	public function ec(){
		if(!$this->session->userdata('logged_in')) {
			redirect('login/');
		}else{
			$this->load->view('layout/pheader', $this->head);
			$this->load->view('pegawai/ec_view');
			$this->load->view('layout/pfooter');
		}
	}


	/*aktivitas ec manager*/
	public function ec_manager_act($comm){
		if(!$this->session->userdata('logged_in')) {
			redirect('login/');
		}else{
			if($comm == "add"){	
				$this->load->view('layout/pheader', $this->head);
				$this->load->view('pegawai/ec_manager_view');
				$this->load->view('layout/pfooter');
			}else if($comm == "add2"){
				$this->load->view('layout/pheader', $this->head);
				$this->load->view('pegawai/ec_manager_acc_view');
				$this->load->view('layout/pfooter');
			}
		}
	}
	
	/*for load bt view*/
	public function bt(){
		if(!$this->session->userdata('logged_in')) {
			redirect('login/');
		}else{
			$this->load->view('layout/pheader', $this->head);
			$this->load->view('pegawai/bt_view');
			$this->load->view('layout/pfooter');
		}
	}

	/*for load spj view*/
	public function spj(){
		if(!$this->session->userdata('logged_in')) {
			redirect('login/');
		}else{
			$this->load->view('layout/pheader', $this->head);
			$this->load->view('pegawai/spj_view');
			$this->load->view('layout/pfooter');
		}
	}

	public function ecAct(){
		$foto;
		$temp=0;

		for ($i=1; $i<=3 ; $i++) { 
			var_dump("hhh".$temp."rrr".$i);

	        //$config['encrypt_name'] = TRUE; //nama yang terupload nantinya
			if($i == 1){
	        	$config['upload_path'] = './images/transportasi/'; //path folder
	        	$config['allowed_types'] = 'jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	        	
	        	$config['file_name'] = date('Y-m-d').'_transportasi_ec'.'_'.$this->session->usernames;       	        
	        	
	        	$this->load->library('upload',$config);
	        	$this->upload->initialize($config);
	        }else if($i == 2){
	        	$config['upload_path'] = './images/parkir_tol/'; //path folder
	        	$config['allowed_types'] = 'jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	        	$config['file_name'] = date('Y-m-d').'_parkir_tol_ec'.'_'.$this->session->usernames; 
	        	$temp = $temp+1;
	        	$this->load->library('upload',$config);
	        	$this->upload->initialize($config);
	        }else if($i == 3){
	        	$config['upload_path'] = './images/lain_lain/'; //path folder
	        	$config['allowed_types'] = 'jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	        	$config['file_name'] = date('Y-m-d').'_lain_lain_ec'.'_'.$this->session->usernames; 
	        	$temp = 0;
	        	$this->load->library('upload',$config);
	        	$this->upload->initialize($config);
	        }
	        
	        //$this->load->library('upload',$config);
	        if(!empty($_FILES['filefoto'.$i]['name'])){
	        	if(!$this->upload->do_upload('filefoto'.$i))
	        		$this->upload->display_errors();  
	        	else
	        		$foto[$i] = $this->upload->data('file_name');
	        }
	        
	    }
        //$data['id_user'] = $this->id;
        //$data['judul'] = set_value('judul');
        /*$data['gambar_1'] = $foto[1];
        $data['gambar_2'] = $foto[2];
        $data['gambar_3'] = $foto[3];
        $data['gambar_4'] = $foto[4];
        $data['gambar_5'] = $foto[5];
        $data['gambar_6'] = $foto[6];*/

        $data = array(
        	'uang_makan_ec' => (int)$this->input->post('uang_makan_ec'),
        	'transportasi_ec' => (int)$this->input->post('transportasi_ec'),
        	'parkir_tol_ec' => (int)$this->input->post('parkir_tol_ec'),
        	'lain_lain_ec' => (int)$this->input->post('lain_lain_ec'),        	
        	'bukti_transportasi_ec' => $foto[1],
        	'bukti_parkir_ec' => $foto[2],
        	'bukti_lain_ec' => $foto[3],
        	'periode_ec' => "test",
        	'status_acc_ec' => "test",
        	'business_trip' => 12,
        	'id_pegawai' => $_SESSION['id_pegawai']
        	);
		//$data = $this->input->post();
		//$data['transportasi_ec'] = (int)$this->input->post('transportasi_ec');
		//$data['uang_makan_ec'] = (int)$this->input->post('uang_makan_ec');
        var_dump($data);
        $this->M_ec->insert($data);
        redirect('Home');
    }

}