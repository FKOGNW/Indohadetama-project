<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index(){

		if($this->session->kategori == 'pegawai')
			redirect('Home');
		elseif($this->session->kategori == 'admin')
			redirect('Admin');

		$this->load->view('login_view');
		
	}

	public function p_login(){
		//print_r($this->input->post());
		//var_dump($this->input->post('username'));
		if($this->M_login->checkLogin($this->input->post('username'),($this->input->post('password')))){
			$dataz = $this->M_login->selectByUsername($this->input->post('username'))->row_array();
			$userdata = array(
				'id_pegawai'  => $dataz['id_pegawai'],
				'nama_pegawai' =>$dataz['nama_pegawai'],
				'username'  => $dataz['username'],
				'departemen' => $dataz['departemen'],
				'jabatan' => $dataz['jabatan'],
				'kategori'  => $dataz['kategori'],			
				'logged_in' => true
			);

			/*insert date and time user login*/
			$now = new DateTime();
			$now->setTimezone(new DateTimezone('Asia/Jakarta'));
			$data['username']= $this->input->post('username');
			$data['last_login']= $now->format('Y-m-d H:i:s');
			$this->M_login->lastLogin($data);

			$this->session->set_userdata($userdata);
			if($this->session->kategori == 'pegawai')
				redirect('Home');
			elseif($this->session->kategori == 'admin')
				redirect('Admin');
		}else{
			redirect('/');
		}
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect(site_url('Login/'));
	}

}