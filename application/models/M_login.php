<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_login extends CI_Model {

	public $tableName;

	public function __construct(){
		parent::__construct();
		$this->tableName = "tbpegawai";
	}

	public function selectByUsername($username){
		$this->db->select('*');
		$this->db->from($this->tableName);
		$this->db->where('username',$username);
		// $this->db->limit($from,$offset);

		return $this->db->get();
	}
	
	public function checkLogin($username,$pwd){
		$this->db->select('*');
		$this->db->from($this->tableName);
		$this->db->where('username',$username);
		$this->db->where('password',$pwd);

		if($this->db->get()->num_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	public function lastLogin($data){
		$this->db->where('username', $data['username']);
	    $this->db->set('last_login', $data['last_login']);
	    $this->db->update('tbpegawai');
	}
}
