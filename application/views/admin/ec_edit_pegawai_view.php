<style>
  body {font-family: Arial, Helvetica, sans-serif;}

  /* The Modal (background) */
  .modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
  }

  /* Modal Content */
  .modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 80%;
  }

  /* The Close Button */
  .close {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
  }

  .close:hover,
  .close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
  }

</style>     


<div class="right_col" role="main">
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Form Edit <small>Reyhan Maulana</small></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <br />
      <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Business Trip
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="first-name"  class="form-control col-md-7 col-xs-12" required >
          </div>
         
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Uang Makan 
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" id="last-name" name="last-name"  class="form-control col-md-7 col-xs-12" require>
          </div>
          
        </div>
        <div class="form-group">
          <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Transportasi</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="middle-name" required>
          </div>
          <a id="myBtn"><button class="btn btn-info btn-xs" > <span class=" glyphicon glyphicon-picture" aria-hidden="true"></span></button></a>
        </div>        
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Parkir dan Tol 
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input id="birthday" class="date-picker form-control col-md-7 col-xs-12"  type="text" required>
          </div>
          <a id="myBtn"><button class="btn btn-info btn-xs" > <span class=" glyphicon glyphicon-picture" aria-hidden="true"></span></button></a>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Lain-lain 
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input id="birthday" class="date-picker form-control col-md-7 col-xs-12"  type="text" required>
          </div>
          <a id="myBtn"><button class="btn btn-info btn-xs" > <span class=" glyphicon glyphicon-picture" aria-hidden="true"></span></button></a>
        </div>
        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <a href="<?php echo site_url('Admin/ecAct/add'); ?>" ><button class="btn btn-primary" type="button">Cancel</button></a>
            <a href="<?php echo site_url('Admin/ecAct/add'); ?>" ><button class="btn btn-success" type="button">Submit</button></a>
            
          </div>
        </div>

      </form>
    </div>
  </div>
</div>

<!-- The Modal -->
    <div id="myModal" class="modal">

      <!-- Modal content -->
      <div class="modal-content">

        <span class="close">&times;</span>
        <img src="<?php echo base_url(); ?>assets/img/user/img.jpg" class="center">
      </div>

    </div>




    <script>
// Get the modal


var modal = document.getElementById('myModal');



// Get the button that opens the modal
var btn = document.getElementById("myBtn");



// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}




</script>

