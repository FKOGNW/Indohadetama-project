        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Data Pegawai<small></small></h3>
              </div>

            </div>

            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12">
                <a href="<?php echo site_url('admin/kelasAct/add');?>" title="Materi"><button class="btn btn-primary" ><span class="fa fa-plus" aria-hidden="true"></span>&nbsp;Tambah Pegawai</button></a>                                     
                <table class="table table-bordered" style="color:black;" id="dt">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>Nama Pegawai</th>
                      <th>Departemen</th>
                      <th>Jabatan</th>
                      <th>Golongan</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>

                  <?php

                  $i=1;
                  foreach($d_ec->result_array() as $row){
                    ?>

                    <tbody>                      
                      <td><?php echo $i; ?></td>
                      <td><?php echo $row['uang_makan_ec']; ?></td>
                      <td><?php echo $row['transportasi_ec']; ?></td>
                      <td>
                        <img src="<?php echo base_url() . 'images/makan/'.$row['bukti_makan_ec'] ?>" alt="Lights" class="img-thumbnail" style="width:150px">
                      </td>
                      <td>g</td>
                      <td>
                        <a href="" title="Materi"><button class="btn btn-warning" ><span class="fa fa-pencil" aria-hidden="true"></span></button></a>
                        <a onclick="" title="Tugas"><button class="btn btn-danger" ><span class="fa fa-trash" aria-hidden="true"></span></button></a>
                      </td>
                    </tbody>

                    <?php } ?>
                  <!-- <tbody>
                    <?php $i=1;foreach($kelas as $list){ ?>
                    <tr>
                      <td><?php echo $i++; ?></td>
                      <td><?php echo $list['nama']; ?></td>
                      <td><?php echo $list['tingkat']; ?></td>
                      <td>
                      <a href="<?php echo site_url('guru/materi/');?>" title="Materi"><button class="btn btn-warning" ><span class="fa fa-pencil" aria-hidden="true"></span></button></a>
                      <a onclick="deletes(<?php echo $list['id']; ?>)" title="Tugas"><button class="btn btn-danger" ><span class="fa fa-trash" aria-hidden="true"></span></button></a>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody> -->
                </table>
                <script type="text/javascript">
                  var url="<?php echo site_url();?>";
                  function deletes(id){
                    swal({
                      title: "Are you sure?",
                      text: "You will not be able to recover this data again!",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Yes, delete it!",
                      closeOnConfirm: false
                    },
                    function(){
                      window.location = url+"admin/kelasAct/del/"+id;
                    });
                  }
                </script>                              
              </div>

            </div>

            
          </div>
        </div>
        <!-- /page content -->

