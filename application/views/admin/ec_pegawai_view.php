 

<div class="right_col" role="main">





  <!-- /page content -->
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Input <small>Periode</small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          <table class="table table-bordered" style="color:black;" id="tMateri">
            <?php echo form_open('Admin/ecAct/add3');?>
            <tbody>
              <tr>
                <td>
                  <label class="control-label col-md-6 col-sm-3 col-xs-12" for="first-name">Mulai</label>
                </td>
                <td><input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="awal_periode"></td>                      
              </tr>
              <tr>
                <td>
                  <label class="control-label col-md-6 col-sm-3 col-xs-12" for="first-name">Akhir</label>
                </td>
                <td><input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="akhir_periode"></td>                      
              </tr>

              <tr>
                <td></td>
                <td><input type="submit" value="Submit" class="btn btn-success"></td>
              </tr>                        
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <div class="clearfix"></div>

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12"> 
      <div class="row x_title">
        <div class="col-md-6">
          <h3>Employer Cost (EC)</h3>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Projects</h2>
              <ul class="nav navbar-right panel_toolbox">
                <li>
                  <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Periode</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <select class="form-control">
                      <?php

                       
                      
                        foreach ($d_periode as $key) {
                        $tanggal = date("dd/mm/YY", $key['awal_periode']);
                          
                        
                      ?>
                        <option><?php echo $key['awal_periode'];   ?></option>
                        
                        <?php
                        
                      }
                        ?> 
                      </select>
                    </div>
                  </div>
                </li>
                
                
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">



              <!-- start project list -->
              <table class="table table-striped projects">
                <thead>
                  <tr>
                    <th style="width: 1%">No</th>
                    <th style="width: 19%">Nama</th>
                    <th style="width: 10%">Bussiness Trip</th>
                    <th style="width: 10%">Uang Makan</th>
                    <th style="width: 10%">Transportasi</th>
                    <th style="width: 10%">Parkir dan Tol</th>
                    <th style="width: 10%">Lain-lain</th>
                    <th style="width: 10%">Biaya</th>
                    <th style="width: 10%">Detail</th>
                  </tr>
                </thead>

                <tbody>
                  <?php
                  $temp="";
                  $tgl=24;
                  for($i=1; $i<=6; $i++){
                    if($i==1){
                      $temp = "Reyhan Maulana";
                    }else if($i==2){
                      $temp = "Enjang Firmansyah";
                    }else if($i==3){
                      $temp = "Wiryan Tirtawan";
                    }else if($i==4){
                      $temp = "Siti Ati Khotimah";
                    }else if($i==5){
                      $temp = "Dani Ismail";
                    }else if($i==6){
                      $temp = "Gilang Surya Putra";
                    }

                    ?>
                    <tr>
                      <td><?php echo $i; ?></td>
                      <td>
                        <a><?php echo $temp; ?></a>
                      </td>
                      <td>
                        <a>-</a>
                      </td>
                      <td>
                        <a>-</a>
                      </td>
                      <td>
                        <a>-</a>
                      </td>
                      <td>
                        <a>-</a>
                      </td>
                      <td>
                        <a>-</a>
                      </td>
                      <td>
                        <a>-</a>
                      </td>
                      <td>

                        <button class="btn btn-info btn-xs"><a href="<?php echo site_url('Admin/ecAct/add'); ?>"><i class="fa fa-pencil"></i> Detail </a></button>


                      </td>
                    </tr>

                    <?php $tgl++; }   ?>          
                  </tbody>
                </table>
                <!-- end project list -->

              </div>
            </div>

          </div>
          <br />
        </div>