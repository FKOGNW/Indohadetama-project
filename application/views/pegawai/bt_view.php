<style>
body {font-family: Arial, Helvetica, sans-serif;}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 80%;
}

/* The Close Button */
.close {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}
</style>        




        <div class="right_col" role="main">

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="row x_title">
                <div class="col-md-6">
                  <h3>Business Trip</h3>
                </div>
              </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>29 - 31 Agustus<small>RSUD Pantura MA SENTOT dan RSUD Indramayu</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
   
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                      Mission : Team Support
                    </p>
                    <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
                      <thead>
                        <tr>
                          <th width="5%">No. </th>
                          <th width="30%">Expanse Type</th>
                          <th width="5%">Qty</th>
                          <th width="10%">Unit Cost</th>
                          <th width="10%">Cost (Rp)</th>
                          <th>Note</th>
                        </tr>
                      </thead>


                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>BT Type 2</td>
                          <td>3</td>
                          <td>125.000</td>
                          <td>375.000</td>
                          <td></td>
                        </tr>

                        <tr>
                          <td>2</td>
                          <td>Uang Kemahalan</td>
                          <td>1</td>
                          <td>50.000</td>
                          <td>50.000</td>
                          <td></td>
                        </tr>

                        <tr>
                          <td>3</td>
                          <td>Travel</td>
                          <td>2</td>
                          <td>90.000</td>
                          <td>180.000</td>
                          <td></td>
                        </tr>

                        <tr>
                          <td>4</td>
                          <td>BT Type 2 (> 7 Jam On Site)</td>
                          <td>2</td>
                          <td>125.000</td>
                          <td>250.000</td>
                          <td></td>
                        </tr>

                      </tbody>
                    </table>

                     <!-- top tiles -->
          <div class="row tile_count">
            <div class="col-md-4 col-sm-4 col-xs-12 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Cost</span>
              <div class="count">Rp. 855.000</div>
              <span class="count_bottom"> Realisasi Biaya Bussiness Trip</span>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 tile_stats_count">
              <span class="count_top"><i class="fa fa-clock-o"></i> Deposit</span>
              <div class="count">Rp. 1.140.000</div>
              <span class="count_bottom"> Rencana Biaya Bussiness Trip</span>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Balance</span>
              <div class="count green">Rp. 285.000</div>
              <span class="count_bottom"> Biaya Pengembalian</span>
            </div>
          </div>
          <!-- /top tiles -->
                  </div>
                </div>
              </div>
          </div>
        </div>


              
<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>

