<style>
  body {font-family: Arial, Helvetica, sans-serif;}

  /* The Modal (background) */
  .modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
  }

  /* Modal Content */
  .modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 80%;
  }

  /* The Close Button */
  .close {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
  }

  .close:hover,
  .close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
  }
</style>        




<div class="right_col" role="main">





  <!-- /page content -->




  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="row x_title">
        <div class="col-md-6">
          <h3>Employer Cost (EC)</h3>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Daily Cost<small>24 September 2018 - 19 September 2018</small></h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">



              <!-- start project list -->
              <table class="table table-striped projects">
                <thead>
                  <tr>
                    <th style="width: 1%">#</th>
                    <th style="width: 19%">Hari/Tanggal</th>
                    <th style="width: 10%">Bussiness Trip</th>
                    <th style="width: 10%">Uang Makan</th>
                    <th style="width: 10%">Transportasi</th>
                    <th style="width: 10%">Parkir dan Tol</th>
                    <th style="width: 10%">Lain-lain</th>
                    <th style="width: 10%">Biaya</th>
                    <th style="width: 10%">Action</th>
                  </tr>
                </thead>

                <tbody>
                  <?php
                  $temp="";
                  $tgl=24;
                  for($i=1; $i<=6; $i++){
                    if($i==1){
                      $temp = "Senin";
                    }else if($i==2){
                      $temp = "Selasa";
                    }else if($i==3){
                      $temp = "Rabu";
                    }else if($i==4){
                      $temp = "Kamis";
                    }else if($i==5){
                      $temp = "Jum'at";
                    }else if($i==6){
                      $temp = "Sabtu";
                    }

                    ?>
                    <tr>
                      <td>#</td>
                      <td>
                        <a><?php  echo $temp .  " " . $tgl;  ?> September 2018</a>
                      </td>
                      <td>
                        <a>-</a>
                      </td>
                      <td>
                        <a>-</a>
                      </td>
                      <td>
                        <a>-</a>
                      </td>
                      <td>
                        <a>-</a>
                      </td>
                      <td>
                        <a>-</a>
                      </td>
                      <td>
                        <a>-</a>
                      </td>
                      <td>
                      <button id="myBtn">EDIT</button>

                      </td>
                    </tr>

                    <?php $tgl++; }   ?>          
                  </tbody>
                </table>
                <!-- end project list -->

              </div>
            </div>

          </div>
          <br />
        </div>

        <button id="myBtn" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Input </button>

        <!-- The Modal -->
        <div id="myModal" class="modal">

          <!-- Modal content -->
          <div class="modal-content">
            <span class="close">&times;</span>
            <!-- ====================== -->

            <table class="table table-bordered" style="color:black;" id="tMateri">
              <?php echo form_open_multipart('Home/ecAct');?>
              <tbody>
                <tr>
                  <td>
                    <label class="control-label col-md-6 col-sm-3 col-xs-12" for="first-name">Bussiness Trip</label>
                  </td>
                  <td><input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" ></td>                      
                </tr>
                <tr>
                  <td>
                    <label class="control-label col-md-6 col-sm-3 col-xs-12" for="first-name">Uang Makan</label>
                  </td>
                  <td><input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="uang_makan_ec"></td>                      
                </tr>
                <tr>
                  <td>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Transportasi</label>
                  </td>
                  <td>
                    <input type="text" id="last-name" required="required" name="transportasi_ec" class="form-control col-md-7 col-xs-12">
                    <input type="file" data-role="magic-overlay" data-target="#pictureBtn" name="filefoto1" data-edit="insertImage" />     
                  </td>                      
                </tr>   
                <tr>
                  <td>
                    <label for="middle-name" class="control-label col-md-6 col-sm-3 col-xs-12">Parkir dan Tol</label>
                  </td>
                  <td>
                    <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="parkir_tol_ec">
                    <input type="file" data-role="magic-overlay" data-target="#pictureBtn" name="filefoto2" data-edit="insertImage" />    
                  </td>                      
                </tr>
                <tr>
                  <td>
                    <label for="middle-name" class="control-label col-md-6 col-sm-3 col-xs-12">Lain-lain</label>
                  </td>
                  <td>
                    <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="lain_lain_ec">
                    <input type="file" data-role="magic-overlay" data-target="#pictureBtn" name="filefoto3" data-edit="insertImage" />    
                  </td>                      
                </tr>                                      
                <tr>
                  <td></td>
                  <td><input type="submit" value="Upload" class="btn btn-primary"></td>
                </tr>                        
              </tbody>
            </table>
            <!-- =========================== -->

          </div>

        </div>
        <script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>

<!-- jQuery -->
<script src="../../assets/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="../../assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../../assets/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="../../assets/vendors/nprogress/nprogress.js"></script>
<!-- bootstrap-progressbar -->
<script src="../../assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="../../assets/vendors/iCheck/icheck.min.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../../assets/vendors/moment/min/moment.min.js"></script>
<script src="../../assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap-wysiwyg -->
<script src="../../assets/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
<script src="../../assets/vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
<script src="../../assets/vendors/google-code-prettify/src/prettify.js"></script>
<!-- jQuery Tags Input -->
<script src="../../assets/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
<!-- Switchery -->
<script src="../../assets/vendors/switchery/dist/switchery.min.js"></script>
<!-- Select2 -->
<script src="../../assets/vendors/select2/dist/js/select2.full.min.js"></script>
<!-- Parsley -->
<script src="../../assets/vendors/parsleyjs/dist/parsley.min.js"></script>
<!-- Autosize -->
<script src="../../assets/vendors/autosize/dist/autosize.min.js"></script>
<!-- jQuery autocomplete -->
<script src="../../assets/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
<!-- starrr -->
<script src="../../assets/vendors/starrr/dist/starrr.js"></script>
<!-- Custom Theme Scripts -->
<script src="../../assets/build/js/custom.min.js"></script>

