-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 24, 2018 at 04:26 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbindohadetama`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbakomodasi`
--

CREATE TABLE `tbakomodasi` (
  `id_akomodasi` int(11) NOT NULL,
  `jabatan_akomodasi` varchar(20) NOT NULL,
  `biaya_akomodasi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbarsip`
--

CREATE TABLE `tbarsip` (
  `id_arsip` int(11) NOT NULL,
  `id_ec` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `nama_pegawai` int(11) NOT NULL,
  `uang_makan_arsip` int(11) NOT NULL,
  `transportasi_arsip` int(11) NOT NULL,
  `parkir_tol_arsip` int(11) NOT NULL,
  `overtime_arsip` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lain_lain_arsip` int(11) NOT NULL,
  `foto_butkti_arsip` varchar(50) NOT NULL,
  `jam_masuk_arsip` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `jam_keluar_arsip` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status_acc_arsip` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbbt`
--

CREATE TABLE `tbbt` (
  `id_bt` int(11) NOT NULL,
  `id_spj` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `total_cost` int(11) NOT NULL,
  `deposit` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbec`
--

CREATE TABLE `tbec` (
  `id_ec` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `uang_makan_ec` int(11) DEFAULT NULL,
  `transportasi_ec` int(11) NOT NULL,
  `parkir_tol_ec` int(11) NOT NULL,
  `overtime_ec` int(11) NOT NULL,
  `lain_lain_ec` int(11) NOT NULL,
  `bukti_makan_ec` varchar(100) DEFAULT NULL,
  `bukti_transportasi_ec` varchar(20) DEFAULT NULL,
  `bukti_parkir_ec` varchar(20) DEFAULT NULL,
  `bukti_lain_ec` varchar(20) DEFAULT NULL,
  `periode_ec` varchar(20) NOT NULL,
  `jam_masuk_ec` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `jam_keluar_ec` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status_acc_ec` varchar(10) NOT NULL,
  `business_trip` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbec`
--

INSERT INTO `tbec` (`id_ec`, `id_pegawai`, `uang_makan_ec`, `transportasi_ec`, `parkir_tol_ec`, `overtime_ec`, `lain_lain_ec`, `bukti_makan_ec`, `bukti_transportasi_ec`, `bukti_parkir_ec`, `bukti_lain_ec`, `periode_ec`, `jam_masuk_ec`, `jam_keluar_ec`, `status_acc_ec`, `business_trip`) VALUES
(1, 1, 1, 222, 222, 0, 222, '2018-09-20_makan_ec_3.jpg', '2018-09-20_makan_ec_', '2018-09-20_makan_ec_', '2018-09-20_makan_ec_', 'test', '2018-09-20 01:56:57', '0000-00-00 00:00:00', 'test', 12),
(2, 1, 1, 2, 222, 0, 2222, '2018-09-20_makan_ec_5.jpg', '2018-09-20_makan_ec_', '2018-09-20_makan_ec_', '2018-09-20_makan_ec_', 'test', '2018-09-20 02:09:58', '0000-00-00 00:00:00', 'test', 12),
(3, 1, 1, 2, 3, 0, 4, '2018-09-20_makan_ec_.jpg', '2018-09-20_makan_ec_', '2018-09-20_makan_ec_', '2018-09-20_makan_ec_', 'test', '2018-09-20 02:11:09', '0000-00-00 00:00:00', 'test', 12),
(4, 1, 1, 2, 3, 0, 4, '2018-09-20_makan_ec_26.jpg', '2018-09-20_makan_ec_', '2018-09-20_makan_ec_', '2018-09-20_makan_ec_', 'test', '2018-09-20 02:18:48', '0000-00-00 00:00:00', 'test', 12),
(5, 1, 1, 2, 3, 0, 4, '2018-09-20_makan_ec_.jpg', '2018-09-20_makan_ec_', '2018-09-20_makan_ec_', '2018-09-20_makan_ec_', 'test', '2018-09-20 02:23:38', '0000-00-00 00:00:00', 'test', 12),
(6, 1, 1, 2, 2, 0, 2, '2018-09-20_makan_ec_4.jpg', '2018-09-20_makan_ec_', '2018-09-20_makan_ec_', '2018-09-20_makan_ec_', 'test', '2018-09-20 02:26:08', '0000-00-00 00:00:00', 'test', 12),
(7, 1, 1, 2, 2, 0, 2, '2018-09-20_makan_ec_13.jpg', '2018-09-20_makan_ec_', '2018-09-20_transport', '2018-09-20_parkir_to', 'test', '2018-09-20 06:38:17', '0000-00-00 00:00:00', 'test', 12),
(8, 1, 1, 2, 21, 0, 1, '2018-09-20_makan_ec_.jpg', '2018-09-20_makan_ec_', '2018-09-20_transport', '2018-09-20_parkir_to', 'test', '2018-09-20 06:40:05', '0000-00-00 00:00:00', 'test', 12),
(9, 1, 1, 1, 1, 0, 1, '2018-09-20_makan_ec_2.jpg', '2018-09-20_makan_ec_', '2018-09-20_transport', '2018-09-20_parkir_to', 'test', '2018-09-20 06:41:01', '0000-00-00 00:00:00', 'test', 12),
(10, 1, 1, 2, 3, 0, 4, '2018-09-20_makan_ec_4.jpg', '2018-09-20_makan_ec_', '2018-09-20_transport', '2018-09-20_parkir_to', 'test', '2018-09-20 06:44:59', '0000-00-00 00:00:00', 'test', 12),
(11, 1, 1, 2, 3, 0, 4, '2018-09-20_makan_ec_6.jpg', '2018-09-20_transport', '2018-09-20_parkir_to', '2018-09-20_lain_lain', 'test', '2018-09-20 06:47:31', '0000-00-00 00:00:00', 'test', 12),
(12, 1, 1, 2, 3, 0, 4, '2018-09-20_makan_ec_.jpg', '2018-09-20_transport', '2018-09-20_parkir_to', '2018-09-20_lain_lain', 'test', '2018-09-20 06:48:28', '0000-00-00 00:00:00', 'test', 12),
(13, 1, 1, 2, 2, 0, 1, '2018-09-20_makan_ec_1.jpg', '2018-09-20_transport', '2018-09-20_parkir_to', NULL, 'test', '2018-09-20 06:49:27', '0000-00-00 00:00:00', 'test', 12),
(14, 1, 1, 0, 3, 0, 4444, NULL, NULL, NULL, NULL, 'test', '2018-10-17 03:32:34', '0000-00-00 00:00:00', 'test', 12),
(15, 1, 111, 111, 111, 0, 111, NULL, NULL, NULL, NULL, 'test', '2018-10-17 03:34:42', '0000-00-00 00:00:00', 'test', 12),
(16, 1, 1, 2, 3, 0, 4444, NULL, NULL, NULL, NULL, 'test', '2018-10-17 03:36:49', '0000-00-00 00:00:00', 'test', 12),
(17, 1, 1, 2, 3, 0, 4444, '2018-10-17_makan_ec_.jpg', NULL, NULL, NULL, 'test', '2018-10-17 03:42:06', '0000-00-00 00:00:00', 'test', 12),
(18, 1, 1, 2, 3, 0, 4444, '2018-10-17_transportasi_ec_.jpg', '2018-10-17_parkir_to', '2018-10-17_lain_lain', NULL, 'test', '2018-10-17 03:43:30', '0000-00-00 00:00:00', 'test', 12),
(19, 1, 1, 2, 3, 0, 4444, NULL, '2018-10-17_transport', '2018-10-17_parkir_to', '2018-10-17_lain_lain', 'test', '2018-10-17 03:44:51', '0000-00-00 00:00:00', 'test', 12),
(20, 3, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, 'test', '2018-10-17 06:39:29', '0000-00-00 00:00:00', 'test', 12),
(21, 3, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, 'test', '2018-10-17 06:42:36', '0000-00-00 00:00:00', 'test', 12);

-- --------------------------------------------------------

--
-- Table structure for table `tbpegawai`
--

CREATE TABLE `tbpegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nama_pegawai` varchar(50) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `departemen` varchar(20) NOT NULL,
  `jabatan` varchar(20) NOT NULL,
  `golongan` varchar(4) NOT NULL,
  `kategori` enum('pegawai','admin','') NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbpegawai`
--

INSERT INTO `tbpegawai` (`id_pegawai`, `nama_pegawai`, `username`, `password`, `departemen`, `jabatan`, `golongan`, `kategori`, `last_login`) VALUES
(1, 'trisna', 'trisna', '123', 'hadir', 'asda', '3', 'pegawai', '2018-10-22 13:57:50'),
(2, 'fiko', 'fiko', '123', 'a', 'aaa', 'sas', 'pegawai', '2018-09-20 01:21:10'),
(3, 'admin', 'admin', 'admin', 'da', 'dsa', 'asda', 'admin', '2018-10-23 12:48:57');

-- --------------------------------------------------------

--
-- Table structure for table `tbperiode`
--

CREATE TABLE `tbperiode` (
  `id_periode` int(11) NOT NULL,
  `awal_periode` date NOT NULL,
  `akhir_periode` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbperiode`
--

INSERT INTO `tbperiode` (`id_periode`, `awal_periode`, `akhir_periode`) VALUES
(1, '2018-10-07', '2018-10-14'),
(3, '2018-10-15', '2018-10-22');

-- --------------------------------------------------------

--
-- Table structure for table `tbspj`
--

CREATE TABLE `tbspj` (
  `id_spj` int(11) NOT NULL,
  `id_akomodasi` int(11) NOT NULL,
  `id_tempat` int(11) NOT NULL,
  `id_trip_type` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `misi_perjalanan` varchar(50) NOT NULL,
  `kendaraan` varchar(20) NOT NULL,
  `tempat_berangkat` varchar(25) NOT NULL,
  `tempat_tujuan` varchar(25) NOT NULL,
  `lama_perjalanan` int(11) NOT NULL,
  `waktu_keberangkatan` date DEFAULT NULL,
  `waktu_kembali` date NOT NULL,
  `nominal_bt_type` int(11) NOT NULL,
  `akomodasi` int(11) NOT NULL,
  `biaya_transportasi_ajuan` int(11) NOT NULL,
  `biaya_transportasi_acc` int(11) NOT NULL,
  `nominal_cadangan` int(11) NOT NULL,
  `total_spj` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbtempat`
--

CREATE TABLE `tbtempat` (
  `id_tempat` int(11) NOT NULL,
  `nama_tempat` varchar(25) NOT NULL,
  `kota` varchar(25) NOT NULL,
  `koordinat` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_bt_type`
--

CREATE TABLE `tb_bt_type` (
  `id_type` int(11) NOT NULL,
  `trip_type` varchar(20) NOT NULL,
  `uang_saku` int(11) NOT NULL,
  `nominal_kemahalan` int(11) NOT NULL,
  `jabatan_bt` varchar(20) NOT NULL,
  `total_biaya_bt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbakomodasi`
--
ALTER TABLE `tbakomodasi`
  ADD PRIMARY KEY (`id_akomodasi`);

--
-- Indexes for table `tbarsip`
--
ALTER TABLE `tbarsip`
  ADD PRIMARY KEY (`id_arsip`);

--
-- Indexes for table `tbbt`
--
ALTER TABLE `tbbt`
  ADD PRIMARY KEY (`id_bt`);

--
-- Indexes for table `tbec`
--
ALTER TABLE `tbec`
  ADD PRIMARY KEY (`id_ec`);

--
-- Indexes for table `tbpegawai`
--
ALTER TABLE `tbpegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `tbperiode`
--
ALTER TABLE `tbperiode`
  ADD PRIMARY KEY (`id_periode`);

--
-- Indexes for table `tbspj`
--
ALTER TABLE `tbspj`
  ADD PRIMARY KEY (`id_spj`);

--
-- Indexes for table `tbtempat`
--
ALTER TABLE `tbtempat`
  ADD PRIMARY KEY (`id_tempat`);

--
-- Indexes for table `tb_bt_type`
--
ALTER TABLE `tb_bt_type`
  ADD PRIMARY KEY (`id_type`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbakomodasi`
--
ALTER TABLE `tbakomodasi`
  MODIFY `id_akomodasi` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbarsip`
--
ALTER TABLE `tbarsip`
  MODIFY `id_arsip` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbbt`
--
ALTER TABLE `tbbt`
  MODIFY `id_bt` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbec`
--
ALTER TABLE `tbec`
  MODIFY `id_ec` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `tbpegawai`
--
ALTER TABLE `tbpegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbperiode`
--
ALTER TABLE `tbperiode`
  MODIFY `id_periode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbspj`
--
ALTER TABLE `tbspj`
  MODIFY `id_spj` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbtempat`
--
ALTER TABLE `tbtempat`
  MODIFY `id_tempat` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_bt_type`
--
ALTER TABLE `tb_bt_type`
  MODIFY `id_type` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
